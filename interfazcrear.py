import getpass
from cryptography.fernet import Fernet
from Controladores.usuario import validacion
#Función para que no hayan campos vacios

def loadKey():
    '''
    load the key from the secret.key file
    '''
    return open("secret.key", "rb").read()
def encryptPass(password):
    '''
    encryp a new password
    '''
    
    # load the key for encrypt
    key = loadKey()
    # use the key with cryptography library
    f = Fernet(key)
    # encode the string to utf-8 for to may encrypt it
    encodedPass = password.encode()
    # encryp the password
    encryptedPass = f.encrypt(encodedPass)
    # return the password encrypted decoded from utf-8
    return encryptedPass.decode('UTF-8')


def registro():
    nombre=input("Digite nombre ")
    usuario=input("Digite usuario ")
    password=getpass.getpass("Digite contraseña ")
 
    
    while True:
        if nombre =="" or usuario =="" or password == "":
            print("No se admiten campos vacios ")
            nombre=input("Digite nombre ")
            usuario=input("Digite usuario ")
            password=getpass.getpass("Digite contraseña ")

            

        else:
            break

    return nombre,usuario,password

#Función para que no hayan campos vacios
def login():
    usuario=input("Digite usuario ")
    password=getpass.getpass("Digite contraseña ")
    while True:
        if usuario=="" or password == "":
            print("No se admiten campos vacios")
            usuario=input("Digite usuario ")
            password=getpass.getpass("Digite contraseña ")
        else:
            break
    while False:
        if validacion==False:
            usuario=input("Digite usuario ")
            password=getpass.getpass("Digite contraseña ")
        else:
            break


    return usuario,password


#Función para que no hayan campos vacios
def aitem():

    print("Un item es un nuevo password de un sitio web ")
    titulo=input("Digite titulo ")
    username=input("Digite usuario ")
    password=getpass.getpass("Digite password ")
    key = loadKey()
    f = Fernet(key)
    encodedPass = password.encode()
    encryptedPass = f.encrypt(encodedPass)
    url=input("Digite URL del sitio web  ")
    comentario=input("Digite algun comentario o recordatorio  ")
    while True:
        if titulo==""or username==""or password==""or url=="":
            print("No se admiten campos vacios")
            titulo=input("Digite titulo ")
            username=input("Digite usuario ")
            password=getpass.getpass("Digite password ")
            key = loadKey()
            f = Fernet(key)
            encodedPass = password.encode()
            encryptedPass = f.encrypt(encodedPass)
            url=input("Digite URL del sitio web  ")

            
        else:
            break

    return titulo,username,encryptedPass.decode('UTF-8'),url,comentario

#Función para que no hayan campos vacios y editar item
def eitem():
    print("Aqui va a editar un item")
    while True:

            
        titulo=input("Digite titulo ")
        username=input("Digite usuario ")
        password=getpass.getpass("Digite password ")
        key = loadKey()
        f = Fernet(key)
        encodedPass = password.encode()
        encryptedPass = f.encrypt(encodedPass)
        url=input("Digite URL del sitio web  ")
        comentario=input("Digite algun comentario o recordatorio  ")
        if titulo==""or username==""or password==""or url=="":
            print("No se admiten campos vacios")
            titulo=input("Digite titulo ")
            username=input("Digite titulo ")
            password=getpass.getpass("Digite password ")
            key = loadKey()
            f = Fernet(key)
            encodedPass = password.encode()
            encryptedPass = f.encrypt(encodedPass)
            url=input("Digite URL del sitio web  ")
            comentario=input("Digite algun comentario o recordatorio  ")
        else:
            break
    return titulo,username,encryptedPass.decode('UTF-8'),url,comentario

#Función para que no hayan campos vacios y agregar tag       
def atag():
    print("El tag es una etiqueta/categoria a un item")
    tag=input("Digite tag ")
    while True:
        if tag=="":
            print("No se admiten campos vacios")
            tag=input("Digite tag ")
        else:
            break
    return tag

def etag():
    print("Aqui va a editar un item")
    while True:
        tag=input("Digite tag")
        if tag=="":
            print("No se admiten campos vacios")
        else:
            break
    return tag
#Función para ver la lista de items
def vitems(itemlist):
    for item in itemlist:
        print(f"id: {item['id']}")   
        print(f"titulo: {item['titulo']}")   
        print(f"url: {item['url']}")   
        print(f"comentario: {item['comentario']}")
        print("")   
#Función para ver un item
def vitem(iteml):
    for item in iteml:
        print(f"id: {item['id']}")   
        print(f"titulo: {item['titulo']}")   
        print(f"username: {item['username']}")   
        print(f"password: {item['password']}")   
        print(f"url: {item['url']}")   
        print(f"comentario: {item['comentario']}")
        print("")

def vtags(itag):
    print(f"tag:{itag['tag']}")
    print("")