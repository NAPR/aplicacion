import json
import uuid

id=str(uuid.uuid4())[:4]
iditems=str(uuid.uuid4())[:6]

#Función para crear el usuario
def crearusuario(nombre,usuario,password):
    #Esto funciona para todo, (edit json)
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    
    data["usuarios"][id] = {
        "id": id,
        "nombre": nombre,
        "usuario": usuario,
        "password":password
    }
    
    #Para volver a guardar en el json
    filer=open("database.json","w")
    json.dump(data,filer,indent = 4, separators = (',', ': '))
    filer.close()

def validarinicio(opc):
    try:
        opc = int(input())
        return True,opc
    except:
        print("Solo numeros ")
        return False,""

#Función para validar el usuario y contraseña
def validacion(usuario,password):
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()

    for user in data["usuarios"]:
        if data["usuarios"][user]["usuario"]==usuario and data["usuarios"][user]["password"]==password:
            print("Usuario aceptado ")
            print("\n")
            return True,user
    print("Usuario o contraseña erronea")
        
    return False,""

            
#Función para agregar un item
def agregaritem(id,titulo,username,password,url,comentario):
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    data["items"][iditems] = {
        "id_usuario":id,
        "tag":"",
        "id":iditems,
        "titulo":titulo,
        "username":username,
        "password":password,
        "url":url,
        "comentario":comentario

    }
    filer=open("database.json","w")
    json.dump(data,filer,indent = 4, separators = (',', ': '))
    filer.close()
def verificaritem():
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    iditems=input("Digite el id del item: ")
    try:
        item = data["items"][iditems]
        return True,iditems
    except:
        print("El item no existe")
        return False,""

#Función para editar un item
def editaritem(iditems,titulo,username,password,url,comentario):
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    data["items"][iditems]["titulo"]=titulo
    data["items"][iditems]["username"]=username
    data["items"][iditems]["password"]=password
    data["items"][iditems]["url"]=url
    data["items"][iditems]["comentario"]=comentario
    


    filer=open("database.json","w")
    json.dump(data,filer,indent = 4, separators = (',', ': '))
    filer.close()
     
#Función para borrar un item
def borraritem(iditems):
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    opc=int(input("Desea eliminar el item: "))
    if opc==1:
        del data["items"][iditems]
        filer=open("database.json","w")
        json.dump(data,filer,indent = 4, separators = (',', ': '))
        filer.close()
    else:
        
        filer=open("database.json","w")
        json.dump(data,filer,indent = 4, separators = (',', ': '))
        filer.close()


#Función para ver la lista de items
def veritems(id):
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    itemlist=[]
    for item in data["items"]:
        if data["items"][item]["id_usuario"]==id:
            itemlist.append(data["items"][item])
    return itemlist


#Función para ver la lista de items
def veritem(id):
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    iteml=[]
    for item in data["items"]:
        if data ["items"][item]["id_usuario"]==id:
            iteml.append(data["items"][item])
    return iteml

#Función para agregar el Tag
def agregartag(iditems,tag):

    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()

    data["items"][iditems]["tag"]=tag
    filer=open("database.json","w")
    json.dump(data,filer,indent = 4, separators = (',', ': '))
    filer.close()

#Función para editar el Tag
def editartag(iditems,tag):

    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()

    data["items"][iditems]["tag"]=tag
    filer=open("database.json","w")
    json.dump(data,filer,indent = 4, separators = (',', ': '))
    filer.close()
#Función para ver la lista de Tags
def vertags(id):

    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    itag=[]
    for item in data["items"]:
        if data["items"][item]["id_usuario"]==id:
            itag.append(data["items"][item]["tag"])
    return itag
#Función para eliminar un Tag
def eliminartag(iditems):
    filer=open("database.json","r")
    data=json.load(filer)
    filer.close()
    
    opc=int(input("Desea eliminar el tag: "))
    if opc==1:
        del data["items"][iditems]["tag"]
        filer=open("database.json","w")
        json.dump(data,filer,indent = 4, separators = (',', ': '))
        filer.close()
    else:
        filer=open("database.json","w")
        json.dump(data,filer,indent = 4, separators = (',', ': '))
        filer.close()